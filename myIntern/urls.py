from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
]

admin.site.site_header = "InternShip Admin"
admin.site.index_title = "Welcome to InternShip Researcher Portal"
